ACWISE is a software package which is developed for atmospheric correction of WISE image. For the current version (v0.1),
it can only handle the Level1G images which are geo-referenced radiance.

Author:Yanqun Pan (yp@arctus.ca,yanqun.pan@uqar.ca)
